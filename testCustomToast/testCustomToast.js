import { LightningElement } from 'lwc';
export default class TestCustomToast extends LightningElement {
    showToast(){
        //this.template.querySelector('c-custom-toast-with-custom-timeout').showToast('success','<strong>This is a Success Message.<strong/>','utility:success',10000);
        //this.template.querySelector('c-custom-toast-with-custom-timeout').showToast('error','<strong>This is a Error Message.<strong/>','utility:error',10000);
        //this.template.querySelector('c-custom-toast-with-custom-timeout').showToast('info','<strong>This is a Info Message.<strong/>','utility:info',10000);
        this.template.querySelector('c-custom-toast-with-custom-timeout').showToast('warning','<strong>This is a Warning Message.<strong/>','utility:warning',10000);
    }
}